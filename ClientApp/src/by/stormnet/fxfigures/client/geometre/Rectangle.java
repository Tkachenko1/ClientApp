package by.stormnet.fxfigures.client.geometre;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Rectangle extends Figure {
    private double width;
    private double height;

    private Rectangle(double cX, double cY, double lineWidth, Color color) {
        super(Figure.FIGURE_TYPE_RECTANGLE, cX, cY, lineWidth, color);
    }

    public Rectangle( double cX, double cY, double lineWidth, Color color, double width, double height) {
        this(cX, cY, lineWidth, color);
        this.width = width;
        this.height = height;
    }



    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setFill(color);//setStroke чтобы рисовал контур
        gc.setLineWidth(LineWidth);
        gc.fillRect(cX - width/2,cY - height/2,width,height);//strokeRect

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.width, width) == 0 &&
                Double.compare(rectangle.height, height) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(width, height);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Прямоугольник {");
        sb.append("координата X=").append(cX);
        sb.append(", координата Y=").append(cY);
        sb.append(", ширина=").append(width);
        sb.append(", высота=").append(height);
        sb.append(", толщина отрисовки линии прямоугольника=").append(LineWidth);
        sb.append(", цвет=").append(color);
        sb.append(", площадь прямоугольника=").append(Math.PI *(width*height));
        sb.append('}');
        return sb.toString();
    }
    @Override
    public void printInfo () { System.out.println(toString());}

    @Override
    public double getSquare() {
        double square = width * height;
        return square;

    }


}

