package by.stormnet.fxfigures.client.geometre;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.io.Serializable;
import java.util.Objects;

public abstract class Figure implements Printable, Squareable, Serializable {
    static final long serialVersionUID = 5538700973722429161L+1;
    public static final int FIGURE_TYPE_CIRCLE     = 0;
    public static final int FIGURE_TYPE_RECTANGLE  = 1;
    public static final int FIGURE_TYPE_TRIANGLE   = 2;

    private int type;

    protected double cX;
    protected double cY;
    protected double LineWidth;
    protected transient Color color;

    public Figure(int type, double cX, double cY, double lineWidth, Color color) {
        this.type = type;
        this.cX = cX;
        this.cY = cY;
        this.LineWidth = lineWidth;
        this.color = color;
    }

    public int getType() {
        return type;
    }

    public double getcX() {
        return cX;
    }

    public void setcX(double cX) {
        this.cX = cX;
    }

    public double getcY() {
        return cY;
    }

    public void setcY(double cY) {
        this.cY = cY;
    }

    public double getLineWidth() {
        return LineWidth;
    }

    public void setLineWidth(double lineWidth) {
        LineWidth = lineWidth;
    }

    public Color getColor(String randomColor) {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Figure figure = (Figure) o;
        return type == figure.type &&
                Double.compare(figure.cX, cX) == 0 &&
                Double.compare(figure.cY, cY) == 0 &&
                Double.compare(figure.LineWidth, LineWidth) == 0 &&
                Objects.equals(color, figure.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, cX, cY, LineWidth, color);
    }

    public abstract void draw(GraphicsContext gc);


}
