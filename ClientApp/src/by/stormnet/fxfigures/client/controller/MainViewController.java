package by.stormnet.fxfigures.client.controller;

import by.stormnet.fxfigures.client.ClientApp;
import by.stormnet.fxfigures.client.geometre.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.io.*;
import java.net.URL;
import java.util.*;

public class MainViewController implements Initializable{
    private List<Figure> figures1;
    public Random rnd;
    private List<Figure> figures3;
    private List<Color> colors;
    private List<Double> r1;
    private List<Double> g1;
    private List<Double> b1;
    private List<Double> r2;
    private List<Double> g2;
    private List<Double> b2;

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures1 = new LinkedList<>();
        rnd = new Random(System.currentTimeMillis());
        figures3 = new LinkedList<>();
        colors = new LinkedList<>();
        r1 = new LinkedList<>();
        g1 = new LinkedList<>();
        b1 = new LinkedList<>();
        r2 = new LinkedList<>();
        g2 = new LinkedList<>();
        b2 = new LinkedList<>();
    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }

    public Figure createFigure(double x, double y) {
        Figure figure = null;
        double lineWidth = rnd.nextInt(6);
        double r = rnd.nextDouble();
        double g = rnd.nextDouble();
        double b = rnd.nextDouble();
        r1.add(r);
        g1.add(g);
        b1.add(b);
        Color randomColor = new Color(r, g, b, 1);
        switch (rnd.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                double radius = rnd.nextInt(101);

                figure = new Circle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        radius < 10 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:

                double width = rnd.nextInt(101);
                double height = rnd.nextInt(101);
                figure = new Rectangle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        width < 10 ? 10 : width, height < 10 ? 10 : height);

               break;

            case Figure.FIGURE_TYPE_TRIANGLE:

                double base = rnd.nextInt(101);
                figure = new Triangle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        base < 10 ? 10 : base);
                break;
            default:
                System.out.println("unknown figure type.");
        }
        return figure;
    }


    private void addFigure(Figure figure) {
        figures1.add(figure);
    }

    private void repaint() {

       canvas.getGraphicsContext2D().clearRect(0, 0, ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);

        for (Figure figure : figures1) {
            if (figure != null) {
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    @FXML//Запишем в консоль параметры фигур
    public void onAction(ActionEvent actionEvent) {
        printAllFiguresInfo(figures1);
        printOveralSquare(figures1);
    }
    private void printAllFiguresInfo(List<Figure> printables){
        for (Printable printable : printables) {
            printable.printInfo();
        }
        System.out.println("==================================================================");
    }

    private void printOveralSquare(List<Figure> squareables){
        double OveralSquare = 0.0;
        for (Squareable squareable: squareables){
            OveralSquare = OveralSquare + squareable.getSquare() ;
        }
        System.out.println("Общая площадь всех фигур равна - " + OveralSquare);
        System.out.println("==================================================================");
    }

    @FXML//Отсортируем фигуры
    public void onAction1(ActionEvent actionEvent) {
        Set<Figure> figures2 = new TreeSet<>(new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                return o1.getType() - o2.getType();
            }
        }.thenComparing((o1, o2) -> (int) (o1.getSquare() - o2.getSquare())));
        for (Figure figure : figures1) {
            figures2.add(figure);
        }
        for (Figure n : figures2) {
            System.out.println(n);
        }
    }

    @FXML//Записать все отображенные фигуры в файл
    public void onAction2(ActionEvent actionEvent) {
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//Фигуры.txt")))
        {
            oos.writeObject(figures1);
            System.out.println("==================================================================");
            System.out.println("Файл успешно записан");
            System.out.println("==================================================================");
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @FXML//Проверить наличие файла
    public void onAction3(ActionEvent actionEvent) {
        try (FileInputStream fin=new FileInputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//Фигуры.txt")) {
            System.out.println("==================================================================");
            System.out.println("Файл с записанными фигурами существует");
            System.out.println("==================================================================");
        }
         catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML//Восстановить цвет фигур
    public void onAction4(ActionEvent actionEvent) {
        try (ObjectInputStream R1 = new ObjectInputStream(new FileInputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//R.txt")))
        {r2 = ((LinkedList<Double>) R1.readObject());
        } catch (Exception ex) {System.out.println(ex.getMessage());}
        try (ObjectInputStream G1 = new ObjectInputStream(new FileInputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//G.txt")))
        {g2 = ((LinkedList<Double>) G1.readObject());
        } catch (Exception ex) {System.out.println(ex.getMessage());}
        try (ObjectInputStream B1 = new ObjectInputStream(new FileInputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//B.txt")))
        {b2 = ((LinkedList<Double>) B1.readObject());
        } catch (Exception ex) {System.out.println(ex.getMessage());}
        System.out.println("==================================================================");
        System.out.println("Цвет фигур восстановлен");
        System.out.println("==================================================================");
    }

    @FXML//Извлечение фигур из файла и отображение их на канве
    public void onAction5(ActionEvent actionEvent) {
        // десериализация в новый список
        figures3 = new LinkedList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//Фигуры.txt"))) {
            figures3 = ((LinkedList<Figure>) ois.readObject());//readObject()
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        for (int i = 0; i < r2.size(); i++) {
            figures3.get(i).setColor(new Color(r2.get(i),g2.get(i),b2.get(i),1));
        }

        for (Figure n : figures3){
            n.draw(canvas.getGraphicsContext2D());
        }
        System.out.println("==================================================================");
        for (Figure n : figures3) {
            System.out.println(n);
        }
            System.out.println("==================================================================");
    }

    @FXML//Очистить консоль
    public void OnAction6(ActionEvent actionEvent) {
        canvas.getGraphicsContext2D().clearRect(0, 0, ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);
    }

    @FXML//Записать цвет фигур в файл
    public void OnAction9(ActionEvent actionEvent) {
        try(ObjectOutputStream R = new ObjectOutputStream(new FileOutputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//R.txt")))
        {R.writeObject(r1);}
        catch(Exception ex){System.out.println(ex.getMessage());}
        try(ObjectOutputStream G = new ObjectOutputStream(new FileOutputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//G.txt")))
        {G.writeObject(g1);}
        catch(Exception ex){System.out.println(ex.getMessage());}
        try(ObjectOutputStream B = new ObjectOutputStream(new FileOutputStream("C://Users//HOME-PC//Desktop//Мой проект 5//XXX//B.txt")))
        {B.writeObject(b1);}
        catch(Exception ex){System.out.println(ex.getMessage());}
        System.out.println("==================================================================");
        System.out.println("Цвет фигур в файл записан");
        System.out.println("==================================================================");

    }
}






